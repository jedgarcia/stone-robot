#ifndef BEZIERCURVE_H
#define BEZIERCURVE_H

#include <QGLWidget>
#include <QtOpenGL>
#include <QGLFunctions>
#include <glm/glm.hpp>
#include <vector>
#include <QPoint>
#include <cmath>
#include <qgl.h>
#include "shapes.h"




//Cubic Bezier Curve Class
class BezierCurve
{
public:
    //Variables
    point controlPointVector[4];
    QVector<GLfloat> controlPointVertices;
    QVector<GLfloat> vertices;

    QVector<GLfloat> pointNormals;

    GLuint vertex_array_id;
    GLuint curve_position_buffer;
    GLuint normal_position_buffer;
    GLuint point_position_buffer;
    //Varibles
    int numberOfSegments;

    QVector<GLfloat> pathDerivatives;
    QVector<GLfloat> pathRotationAngles;




    //Functions
    BezierCurve();
    BezierCurve(point p0, point p1, point p2, point p3);
    BezierCurve(point p0, point p1, point p2, point p3, int num);

    void UpdatePoints(point p0, point p1, point p2, point p3);


private:





    QGLShaderProgram* shader;


    //Functions
    void calculateVertices();
    void calculatePointNormals();







};

#endif // BEZIERCURVE_H
