#-------------------------------------------------
#
# Project created by QtCreator 2014-01-21T14:31:03
#
#-------------------------------------------------

QT       += core gui opengl

TARGET = Bezier
TEMPLATE = app


SOURCES += main.cpp\
        window.cpp \
    glwidget.cpp \
    beziercurve.cpp \
    bezierobject.cpp

HEADERS  += window.h \
    glwidget.h \
    beziercurve.h \
    bezierobject.h \
    shapes.h


RESOURCES += \
    shaders.qrc \
    textures.qrc

OTHER_FILES +=
