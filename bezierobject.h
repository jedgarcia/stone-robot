#ifndef BEZIEROBJECT_H
#define BEZIEROBJECT_H
#include "beziercurve.h"

class BezierObject
{
public:

    BezierObject(BezierCurve* curve);
    BezierCurve *curve;



    //Vectors!
    std::vector<GLfloat> vertices_normals;
    std::vector<GLfloat> vertices_normals_top;
    std::vector<GLfloat> vertices_normals_bottom;

    std::vector<GLubyte> indices;
    std::vector<GLubyte> indices_top;
    std::vector<GLubyte> indices_bottom;

    std::vector<GLfloat> textureBuffer;
    std::vector<GLfloat> textureBuffer_top;
    std::vector<GLfloat> textureBuffer_bottom;


    //gloc Id's!
    GLuint position_and_normal_buffer_surface;
    GLuint position_and_normal_buffer_top;
    GLuint position_and_normal_buffer_bottom;

    GLuint vertex_id;
    GLuint vertex_top_id;
    GLuint vertex_bottom_id;

    GLuint index_buffer;
    GLuint index_buffer_top;
    GLuint index_buffer_bottom;

    GLuint textureBuffer_id;
    GLuint textureBuffer_top_id;
    GLuint textureBuffer_bottom_id;



    GLuint num_indices_surface;
    GLuint num_indices_top;
    GLuint num_indices_bottom;








};

#endif // BEZIEROBJECT_H
