#include "beziercurve.h"

BezierCurve::BezierCurve(): numberOfSegments(10)
{

    for(int i = 0; i < 4; i++){
        controlPointVector[i] = point(0,0,0);

        //X Y Z
        controlPointVertices.append(0.00);
        controlPointVertices.append(0.00);
        controlPointVertices.append(0.00);
    }

    calculateVertices();
    calculatePointNormals();

}

BezierCurve::BezierCurve(point p0, point p1, point p2, point p3): numberOfSegments(10){

    controlPointVector[0] = p0;
    controlPointVector[1] = p1;
    controlPointVector[2] = p2;
    controlPointVector[3] = p3;

    for(int i = 0 ; i < 4; i++){
        controlPointVertices.append(controlPointVector[i].x);
        controlPointVertices.append(controlPointVector[i].y);
        controlPointVertices.append(controlPointVector[i].z);
    }


    calculateVertices();
    calculatePointNormals();

}

BezierCurve::BezierCurve(point p0, point p1, point p2, point p3, int num): numberOfSegments(num)
{
    controlPointVector[0] = p0;
    controlPointVector[1] = p1;
    controlPointVector[2] = p2;
    controlPointVector[3] = p3;

    for(int i = 0 ; i < 4; i++){
        controlPointVertices.append(controlPointVector[i].x);
        controlPointVertices.append(controlPointVector[i].y);
        controlPointVertices.append(controlPointVector[i].z);
    }


    calculateVertices();
    calculatePointNormals();
}


void BezierCurve::calculateVertices(){
    // number of evaluated points is number of segments + 1
    // Cubic Bezier Function: ((1-t)^3 * P0) + ((3(1-t)^2)*t*P1) + (3(1-t)*t^2 * P2) + t^3 * P3
    //for all values [0,1]

    for(int i = 0; i <= numberOfSegments; ++i)
    {
      float t = i/(float)numberOfSegments;

      float one_minus_t = 1 - t;
      float one_minus_t_squared = one_minus_t * one_minus_t;
      float one_minus_t_cubed = one_minus_t * one_minus_t * one_minus_t;
      float t_squared = t*t;
      float t_cubed = t*t*t;

      point p_on_curve = (controlPointVector[0] * one_minus_t_cubed ) +
              (controlPointVector[1] * (3.0 * one_minus_t_squared * t)) +
              (controlPointVector[2] * (3.0 * one_minus_t * t_squared )) +
              (controlPointVector[3] * t_cubed);

      vertices.push_back(p_on_curve.x);
      vertices.push_back(p_on_curve.y);
      vertices.push_back(p_on_curve.z);
    }
}

void BezierCurve::calculatePointNormals(){
    // number of evaluated points is number of segments + 1
    // Cubic Bezier Function:
      // ((1-t)^3 * P0) + ((3(1-t)^2)*t*P1) + (3(1-t)*t^2 * P2) +
      // t^3 * P3
      //for all values [0,1]

    for(int i = 0; i <= numberOfSegments; ++i)
    {

      float t = i/(float)numberOfSegments;
      float one_minus_t = 1 - t;
      float one_minus_t_squared = one_minus_t * one_minus_t;
      float one_minus_t_cubed = one_minus_t * one_minus_t * one_minus_t;
      float t_squared = t*t;
      float t_cubed = t*t*t;


      point p_on_curve = (controlPointVector[0] * one_minus_t_cubed ) +
              (controlPointVector[1] * (3.0 * one_minus_t_squared * t)) +
              (controlPointVector[2] * (3.0 * one_minus_t * t_squared )) +
              (controlPointVector[3] * t_cubed);

      point derivative = (controlPointVector[1] - controlPointVector[0]) * ( 3 * one_minus_t_squared) + (controlPointVector[2]-controlPointVector[1]) * (6*one_minus_t * t) + (controlPointVector[3]-controlPointVector[2]) * (3*t_squared);

      point n(-derivative.y, derivative.x, 0);

      n = n.normalize();
      pointNormals.push_back(p_on_curve.x);
      pointNormals.push_back(p_on_curve.y);
      pointNormals.push_back(p_on_curve.z);
      pointNormals.push_back(p_on_curve.x + n.x);
      pointNormals.push_back(p_on_curve.y + n.y);
      pointNormals.push_back(p_on_curve.z + n.z);

    }
}


void BezierCurve::UpdatePoints(point p0, point p1, point p2, point p3){

    controlPointVertices.clear();
    vertices.clear();
    pointNormals.clear();

    controlPointVector[0] = p0;
    controlPointVector[1] = p1;
    controlPointVector[2] = p2;
    controlPointVector[3] = p3;

    for(int i = 0 ; i < 4; i++){
        controlPointVertices.append(controlPointVector[i].x);
        controlPointVertices.append(controlPointVector[i].y);
        controlPointVertices.append(controlPointVector[i].z);
    }


    calculateVertices();
    calculatePointNormals();
}
























