#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "beziercurve.h"
#include "bezierobject.h"

#include <QGLWidget>
#include <QtOpenGL>
#include <QGLFunctions>
#include <glm/glm.hpp>

#include <vector>
#include <QPoint>

#define PI 3.14159265359

using namespace std;

enum allObject{
    Test,
    WineBottleTop,
    WineBottleBottom,
    Table,
    WineGlassTop,
    WineGlassBottom,
    SecondWineGlassTop,
    SecondWineGlassBottom
};

enum indexForRobot{
    Tester,
    HEAD,
    NECK,
    BODY,
    LEFT_LEG_THIGH,
    LEFT_LEG_SHIN,
    RIGHT_LEG_THIGH,
    RIGHT_LEG_SHIN,
    LEFT_ARM_BICEP,
    LEFT_ARM_FOREARM,
    RIGHT_ARM_BICEP,
    RIGHT_ARM_FOREARM,
    FLASHLIGHT
};

class GLWidget : public QGLWidget, protected QGLFunctions
{
    Q_OBJECT


public:
    PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays;
    PFNGLGENVERTEXARRAYSPROC glGenVertexArrays;
    PFNGLBINDVERTEXARRAYPROC glBindVertexArray;


    explicit GLWidget(QWidget *parent = 0);
    ~GLWidget();

    QSize minimumSizeHint() const;
    QSize sizeHint() const;


    //Bezier Helpers
    void initializeBezierCurve(BezierCurve& object);
    void drawBezierCurve(BezierCurve& object);
    void drawBezierNormalLines(BezierCurve& object);
    void drawBezierPoints(BezierCurve& object);



    void evaluateCamera();

    //from shapes----------------------------------

    void makeCubicBezierCurve(
            std::vector<GLfloat>* vertices,
            point p0, point p1, point p2,point p3,
            int num_segments);


    void makeCubicBezierCurveNormals(
            std::vector<GLfloat>* normals,
            point p0, point p1, point p2, point p3,
            int num_segments);




    void makeBezierSurface(
            std::vector<GLfloat>* vertices_and_normals,
            std::vector<GLubyte>* indices,
            std::vector<GLfloat>* textureCoordinates,
            point p0, point p1, point p2,
            point p3, int num_segments);



    void makeSurface(BezierObject& object);

    void paintBezierObject(        BezierObject &object,
                                   glm::vec3 scale,
                                   glm::vec3 inTranslationMatrix,
                                   glm::vec3 color,
                                   float rotationAngle,
                                   glm::vec3 rotationAxis,
                                   glm::vec3 topOfAppendage,
                                   bool useTopOfAppendage,
                                   bool keepOldModel_matrix,
                                   GLuint textureToUse,
                                   int index
            );

    void makeSurfaceHelperInitilizer(GLuint& vertex_id,

                                     GLuint& position_and_normal_buffer_surface,
                                     vector<GLfloat>& vertices_normals,

                                     GLuint& index_buffer_id,
                                     vector<GLubyte>& indices,

                                     GLuint& textureBuffer_id,
                                     vector<GLfloat>& textureBuffer,

                                     GLuint& num_indices_surface
                                     );

    void makeBezierExtremes(
            std::vector<GLfloat>* vertices_and_normals,
            std::vector<GLubyte>* indices,
            std::vector<GLfloat>* textureCoordinates,
            point p0, point p1,
            point p2, point p3,
            int num_segments,
            bool isTop
            );



    void makePiecewiseObject(
            point p0,
            point p1,
            point p2,
            point p3,
            point p4,
            point p5,
            point p6
            );
    void makePiecewiseObject(
            point p0,
            point p1,
            point p2,
            point p3
            );



signals:

public slots:
    void selectP0();
    void selectP1();
    void selectP2();
    void selectP3();

    void increaseX();
    void decreaseX();
    void increaseY();
    void decreaseY();
    void timerFunc();




protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);
    void keyPressEvent(QKeyEvent *e);

    void mousePressEvent(QMouseEvent *event);


private:
    void initializeShaders();
    void initializeBuffers();
    void updateBezierCurve();
    void evaluateRotations();
    void initializeTextures();
    void initializePathToWalkOn();

    void printVec3(glm::vec3 vectorIn);
    void printControlPoints();
    void printVertices(QString title, QVector<GLfloat>& verticesIn);
    void printVertices(QString title, QVector<int>& verticesIn);
    void drawPath();
    void printBookVariables();
    void makeTangentsToPath(BezierCurve& curve);
    void printPoints(point p);
    void makepathFacingRotations();
    void drawPlane();

    void rotateCamera(float angle, glm::vec3 axis);

    //robot direction
    glm::vec3 currentRobotDirection;

    //drawing
    QVector<glm::vec3> posMatPaint;
    QVector<glm::vec3> scaleMatPaint;
    QVector<glm::vec3> colorMatPaint;
    QVector<float> rotationAngles;
    QVector<glm::vec3> rotMatAxis;
    QVector<glm::vec3> topOfAppendage;
    QVector<bool> useTopOfAppendage;
    QVector<bool> keepOldModelMatrix;

    QVector<int> problemPointIterators;


    void shaderCameraSetUp();




    GLfloat animationCounter;

    bool drawRobot;

    QGLShaderProgram* shader_spotlight;
    QGLShaderProgram* shader_lighting;
    QGLShaderProgram* shader_basic;
    GLuint draw_mode;




    //Camera and view related things


    GLfloat cameraPosZ;
    GLfloat cameraPosY;
    GLfloat cameraPosX;

    glm::vec3 cameraCenterOfAttention;
    glm::vec3 cameraUpVector;

    glm::vec3 cameraPosition;
    glm::mat4 proj_mat;
    glm::mat4 view_mat;
    glm::mat4 model_mat;
    glm::mat3 normal_mat;
    glm::mat4 rotationMatrix;

    glm::mat4 last_model_mat;




    float angle_x, angle_y;
    float x,y,z;
    float aspect_ratio, fieldOfView,zNear,zFar;

    float bookX;
    float bookY;
    float bookZ;

    float sizeOffset;

    //control Points
    QVector<glm::vec3*> ctrl_pt;
    glm::vec3 ctrl_pt0;
    glm::vec3 ctrl_pt1;
    glm::vec3 ctrl_pt2;
    glm::vec3 ctrl_pt3;


    //Holds all the 3d objects made from Bezier Curves
    QVector<BezierObject*> allObjects;

    BezierCurve* pathToWalk;


    //Selector for which point to manipulate
    int selected_point;
    bool showAllCurves;
    bool showTestLine;


        bool justRotatedTheCamera;
        float cameraHeadX;
        float cameraHeadY;
        float cameraHeadZ;



    //Textures
    GLuint flower_texture;


    //Animation
    QTimer* scene_timer;
    float anim_time;
    float anim_step;
    bool anim_stopped;
    float translate_z1;
    float translate_z2;


    glm::vec3 pointOnCurve;
    int pointIterator;


    GLuint plane_vertex;
    GLuint plane_position;
    GLuint plane_normals;


    //spotlight things
    glm::vec3 spot_position;
    glm::vec3 spot_direction;
    GLfloat spot_cos_angle;
    GLfloat spot_exponent;
    GLfloat linear_attenuation;

};

#endif // GLWIDGET_H
