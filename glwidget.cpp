#include "glwidget.h"

#include <GL/glext.h>
#if defined(__linux)
#include <GL/glx.h>
#endif

#include <QtGui>
#include <iostream>
#define GLM_FORCE_RADIANS

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>



GLWidget::GLWidget(QWidget *parent) :
    QGLWidget(parent), draw_mode(GL_TRIANGLES), angle_x(0.f), angle_y(0.f),
    bookX(0.0), bookY(0), bookZ(0),
    ctrl_pt0(glm::vec3(-0.7, 0, 0)),
    ctrl_pt1(glm::vec3(0, 0.6, 0)),
    ctrl_pt2(glm::vec3(0.8, 0.1, 0)),
    ctrl_pt3(glm::vec3(.90, 0, 0)),
    selected_point(0), sizeOffset(0),
    showAllCurves(false),
    showTestLine(true),
    pointIterator(0),
    drawRobot(true),
    animationCounter(0.0),
    currentRobotDirection(glm::vec3(0,0,1)),
    justRotatedTheCamera(false),


    spot_position(glm::vec3(2,2,0)),
    spot_direction(glm::vec3(0,0,1)),

    spot_cos_angle(.95),
    spot_exponent(15),
    linear_attenuation(0)
{
    setFocusPolicy(Qt::StrongFocus);
    setFocus();
    setMouseTracking(true);
    glPointSize(.8);

    if(drawRobot){
        //Animation
        scene_timer = new QTimer();
        connect(scene_timer,SIGNAL(timeout()),this,SLOT(timerFunc()));
        anim_time = 0;
        anim_step = 0.05;
        scene_timer->start(30);
    }


}


GLWidget::~GLWidget(){}

void GLWidget::evaluateRotations(){
    rotationMatrix = glm::rotate(glm::mat4(1), glm::pi<float>() * angle_x, glm::vec3(0,1,0));
    rotationMatrix = glm::rotate(rotationMatrix, glm::pi<float>() * angle_y, glm::vec3(1,0,0));
}

void GLWidget::initializeTextures()
{
    shader_lighting->bind();
    //generate a unique id for the texture
    glGenTextures(1, &flower_texture);

    //set the texture to be the currently active one
    glBindTexture(GL_TEXTURE_2D, flower_texture);

    //set texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    //possible values are GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER, GL_REPEAT, GL_MIRRORED_REPEAT
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    //load the image data into the texture
    QImage img(":/textures/metalTexture.jpg");
    QImage glimg = QGLWidget::convertToGLFormat(img);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, glimg.width(), glimg.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, glimg.bits());

    //connect texture unit 0 to the appropriate shader variable
    GLuint loc = glGetUniformLocation(shader_lighting->programId(), "tex_sampler");
    glUniform1i(loc, 0);
}


void GLWidget::initializeGL()
{
    //load opengl functions that are not in QGLFunctions
#if defined(__linux)
    glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC)glXGetProcAddress((GLubyte*)"glDeleteVertexArrays");
    glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)glXGetProcAddress((GLubyte*)"glGenVertexArrays");
    glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC)glXGetProcAddress((GLubyte*)"glBindVertexArray");
#elif defined(WIN32)
    glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC)wglGetProcAddress("glDeleteVertexArrays");
    glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC)wglGetProcAddress("glGenVertexArrays");
    glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC)wglGetProcAddress("glBindVertexArray");
#endif

    //putting control points in an array for ease of use
    ctrl_pt.append(&ctrl_pt0);
    ctrl_pt.append(&ctrl_pt1);
    ctrl_pt.append(&ctrl_pt2);
    ctrl_pt.append(&ctrl_pt3);

    //load opengl functions
    initializeGLFunctions();

    //set the clear color to White
    glClearColor(0.51372549,0.545098039,0.51372549 ,1);
    //glClearColor(1,1,1 ,1);

    //enable depth testing
    glEnable(GL_DEPTH_TEST);

    //enable back face culling
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    //set point size
    glPointSize(4.0);

    //load and initialize shaders
    initializeShaders();
    initializeTextures();

    initializeBuffers();





    //Defining our camera as positioned at (0,3,5), looking at (0,0,0) and
    //having the top of camera towards the Y Axis
    this->cameraPosX = 0;
    this->cameraPosY = 45;
    this->cameraPosZ = 40;


    cameraPosition = glm::vec3(cameraPosX,cameraPosY,cameraPosZ);
    cameraCenterOfAttention = glm::vec3(0,0,0);
    cameraUpVector = glm::vec3(0,1,0);

    view_mat = glm::lookAt(cameraPosition,cameraCenterOfAttention ,cameraUpVector );




    shaderCameraSetUp();

    shader_spotlight->bind();
    //set the light color
    GLuint loc = glGetUniformLocation(shader_spotlight->programId(), "light_color");
    glUniform4f(loc, 1.f, 1.f, 1.f, 1.f);

    //spotlight position
    loc = glGetUniformLocation(shader_spotlight->programId(), "spot_pos");
    glUniform3f(loc, spot_position.x, spot_position.y, spot_position.z);

    //spotlight direction
    loc = glGetUniformLocation(shader_spotlight->programId(), "spot_dir");
    glUniform3f(loc, spot_direction.x, spot_direction.y, spot_direction.z);

    //spotlight cutoff
    loc = glGetUniformLocation(shader_spotlight->programId(), "spot_cutoff");
    glUniform1f(loc, spot_cos_angle);

    //spotlight exponent
    loc = glGetUniformLocation(shader_spotlight->programId(), "spot_exponent");
    glUniform1f(loc, spot_exponent);

    //linear attenuation
    loc = glGetUniformLocation(shader_spotlight->programId(), "linear_attenuation");
    glUniform1f(loc, linear_attenuation);

    std::cout << (char*)glGetString(GL_VERSION) << std::endl;
}


void GLWidget::shaderCameraSetUp(){
    //-----------------------------------------------------------------------------
    //Spotlight shader setup
    //----------------------------------------------------------------------------
    shader_spotlight->bind();
    GLuint loc = glGetUniformLocation(shader_spotlight->programId(), "view_mat");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(view_mat));


    //lighting Shader Setup
    shader_lighting->bind();
    loc = glGetUniformLocation(shader_lighting->programId(), "view_mat");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(view_mat));

    //basic shader Setup
    shader_basic->bind();
    loc = glGetUniformLocation(shader_basic->programId(), "view_mat");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(view_mat));


}


void GLWidget::resizeGL(int w, int h)
{
    //set the viewing rectangle to be the entire window
    glViewport(0,0,w,h);
    float aspect_ratio = w/(float)h;


    proj_mat = glm::perspective(glm::pi<float>() * 0.25f, aspect_ratio, 0.1f, 150.f);

    //Spotlight Shader
    shader_spotlight->bind();
    GLuint loc = glGetUniformLocation(shader_spotlight->programId(), "proj_mat");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(proj_mat));


    //Lighting Shader
    shader_lighting->bind();
    loc = glGetUniformLocation(shader_lighting->programId(), "proj_mat");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(proj_mat));


    //Basic Shader
    shader_basic->bind();
    loc = glGetUniformLocation(shader_basic->programId(), "proj_mat");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(proj_mat));


}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(500, 500);
}

void GLWidget::initializeShaders()
{
    shader_spotlight = new QGLShaderProgram();
    shader_spotlight->addShaderFromSourceFile(QGLShader::Vertex, ":/spotlight.vert");
    shader_spotlight->addShaderFromSourceFile(QGLShader::Fragment, ":/spotlight.frag");
    shader_spotlight->link();
    shader_spotlight->bind();


    //load the shaders, link the shader, and set the shader to be active
    shader_lighting = new QGLShaderProgram();
    shader_lighting->addShaderFromSourceFile(QGLShader::Vertex, ":/lighting.vert");
    shader_lighting->addShaderFromSourceFile(QGLShader::Fragment, ":/lighting.frag");
    shader_lighting->link();
    shader_lighting->bind();


    shader_basic = new QGLShaderProgram();
    shader_basic->addShaderFromSourceFile(QGLShader::Vertex, ":/basic.vert");
    shader_basic->addShaderFromSourceFile(QGLShader::Fragment, ":/basic.frag");
    shader_basic->link();
    shader_basic->bind();



}

bool animationOn = true;
bool cameraHead = false;
void GLWidget::keyPressEvent(QKeyEvent *e)
{
    switch(e->key())
    {

    case Qt::Key_T:
    {
        cameraHead = !cameraHead;
        if(cameraHead){


        }else{

            //Defining our camera as positioned at (0,3,5), looking at (0,0,0) and
            //having the top of camera towards the Y Axis
            this->cameraPosX = 0;
            this->cameraPosY = 45;
            this->cameraPosZ = 40;


            cameraPosition = glm::vec3(cameraPosX,cameraPosY,cameraPosZ);
            cameraCenterOfAttention = glm::vec3(0,0,0);
            cameraUpVector = glm::vec3(0,1,0);

            view_mat = glm::lookAt(cameraPosition,cameraCenterOfAttention ,cameraUpVector );

            shaderCameraSetUp();
        }
        break;
    }

    case Qt::Key_Space :
    {
        animationOn = !animationOn;
        break;
    }

    case Qt::Key_Down:
    {
        if(cameraHead) break;
        cameraPosition[1] -= 3;
        evaluateCamera();
        break;
    }

    case Qt::Key_Left:
    {
        if(cameraHead) break;
        cameraPosition[0] -= 3;
        evaluateCamera();
        break;
    }

    case Qt::Key_Right:
    {
        if(cameraHead) break;
        cameraPosition[0] += 3;
        evaluateCamera();
        break;
    }
    case Qt::Key_Up:
    {
        if(cameraHead) break;
        cameraPosition[1] += 3;
        evaluateCamera();
        break;

    }


    case Qt::Key_7:
    {
        cameraPosition[2] += 3;
        evaluateCamera();
        break;
    }

    case Qt::Key_9:
    {
        if(cameraPosition[2] >= 0.1)
            cameraPosition[2] -= .15;
        evaluateCamera();
        break;
    }

    case Qt::Key_Plus:
    {
        cameraPosition[1] -= 3;
        cameraPosition[2] -= 3;
        cameraCenterOfAttention[1] -= 3;
        cameraCenterOfAttention[2] -= 3;
        evaluateCamera();
        break;
    }
    case Qt::Key_Minus:
    {
        cameraPosition[1] += 3;
        cameraPosition[2] += 3;
        cameraCenterOfAttention[1] += 3;
        cameraCenterOfAttention[2] += 3;
        evaluateCamera();
        break;
    }

    default:
        QWidget::keyPressEvent(e);
        break;
    }
    //printBookVariables();
    updateGL();
}

void GLWidget::printBookVariables(){
    cout << "BookX: " << bookX << "  BookY: " << bookY << "  BookZ: " <<  bookZ << endl;
}

//-------------------------------------

void GLWidget::selectP0()
{
    selected_point = 0;
}

void GLWidget::selectP1()
{
    selected_point = 1;
}

void GLWidget::selectP2()
{
    selected_point = 2;
}

void GLWidget::selectP3(){
    selected_point = 3;
}


//-------------------------------------

void GLWidget::increaseX()
{
    switch(selected_point)
    {
    case 0:
        ctrl_pt0.x += 0.1;
        break;
    case 1:
        ctrl_pt1.x += 0.1;
        break;
    case 2:
        ctrl_pt2.x += 0.1;
        break;
    case 3:
        ctrl_pt3.x += 0.1;
        break;
    }
    updateBezierCurve();
    updateGL();
}

void GLWidget::decreaseX()
{
    switch(selected_point)
    {
    case 0:
        ctrl_pt0.x -= 0.1;
        break;
    case 1:
        ctrl_pt1.x -= 0.1;
        break;
    case 2:
        ctrl_pt2.x -= 0.1;
        break;
    case 3:
        ctrl_pt3.x -= 0.1;
        break;
    }
    updateBezierCurve();
    updateGL();
}

void GLWidget::increaseY()
{
    switch(selected_point)
    {
    case 0:
        ctrl_pt0.y += 0.1;
        break;
    case 1:
        ctrl_pt1.y += 0.1;
        break;
    case 2:
        ctrl_pt2.y += 0.1;
        break;
    case 3:
        ctrl_pt3.y += 0.1;
        break;
    }
    updateBezierCurve();
    updateGL();
}

void GLWidget::decreaseY()
{
    switch(selected_point)
    {
    case 0:
        ctrl_pt0.y -= 0.1;
        break;
    case 1:
        ctrl_pt1.y -= 0.1;
        break;
    case 2:
        ctrl_pt2.y -= 0.1;
        break;
    case 3:
        ctrl_pt3.y -= 0.1;
        break;
    }
    updateBezierCurve();
    updateGL();
}

void GLWidget::updateBezierCurve()
{
    cout << "updateBezierCurve()" <<  endl;

    allObjects.at(Test)->curve->UpdatePoints(
                point(ctrl_pt0.x, ctrl_pt0.y, ctrl_pt0.z),
                point(ctrl_pt1.x, ctrl_pt1.y, ctrl_pt1.z),
                point(ctrl_pt2.x, ctrl_pt2.y, ctrl_pt2.z),
                point(ctrl_pt3.x, ctrl_pt3.y, ctrl_pt3.z)
                );



    makeSurface(*allObjects.at(Test));


    /*
    cout << endl;
    cout << "allObjects.append(new BezierObject(new BezierCurve(" << endl;
    cout << "point(" << ctrl_pt0.x << ", " << ctrl_pt0.y << ", " << ctrl_pt0.z << ")," << endl;
    cout << "point(" << ctrl_pt1.x << ", " << ctrl_pt1.y << ", " << ctrl_pt1.z << ")," << endl;
    cout << "point(" << ctrl_pt2.x << ", " << ctrl_pt2.y << ", " << ctrl_pt2.z << ")," << endl;
    cout << "point(" << ctrl_pt3.x << ", " << ctrl_pt3.y << ", " << ctrl_pt3.z << ")" << endl;
    cout << "   )));" << endl;
    */
}

void GLWidget::mousePressEvent(QMouseEvent *event){

    if(event->buttons() & Qt::LeftButton){

        QPoint pos = event->pos();

        float mouseX = (pos.x() - (width()/2.0)) ;
        float mouseY = (pos.y() - (height()/2.0));
        mouseX /= (width()/2.0);
        mouseY /= -(height()/2.0);



        switch(selected_point)
        {
        case 0:
            ctrl_pt0.x = mouseX;
            ctrl_pt0.y = mouseY;
            break;
        case 1:
            ctrl_pt1.x = mouseX;
            ctrl_pt1.y = mouseY;
            break;
        case 2:
            ctrl_pt2.x = mouseX;
            ctrl_pt2.y = mouseY;
            break;
        case 3:
            ctrl_pt3.x = mouseX;
            ctrl_pt3.y = mouseY;
            break;
        }
        updateBezierCurve();
        updateGL();

    }
}


//creates a vertex for the Bezier curve, and also some buffers
void GLWidget::initializeBezierCurve(BezierCurve &object){


    //get a unique id for the vertex array
    glGenVertexArrays(1, &(object.vertex_array_id));

    //set the vertex array to be the active one
    glBindVertexArray(object.vertex_array_id);

    //get a unique id for the position vertex buffer
    glGenBuffers(1, &(object.curve_position_buffer));

    //get a unique id for the position vertex buffer
    glGenBuffers(1, &(object.normal_position_buffer));

    //get a unique id for the position vertex buffer
    glGenBuffers(1, &(object.point_position_buffer));

    //get a unique id for the texture vertex buffer
    //glGenBuffers(1, &(object.));


}

void GLWidget::makeCubicBezierCurve(std::vector<GLfloat>* vertices, point p0, point p1, point p2,point p3, int num_segments)
{
    // number of evaluated points is number of segments + 1
    // Cubic Bezier Function:
    // ((1-t)^3 * P0) + ((3(1-t)^2)*t*P1) + (3(1-t)*t^2 * P2) +
    // t^3 * P3
    //for all values [0,1]
    for(int i = 0; i <= num_segments; ++i)
    {
        float t = i/(float)num_segments;

        float one_minus_t = 1 - t;
        float one_minus_t_squared = one_minus_t * one_minus_t;
        float one_minus_t_cubed = one_minus_t * one_minus_t * one_minus_t;
        float t_squared = t*t;
        float t_cubed = t*t*t;

        point p_on_curve = (p0 * one_minus_t_cubed ) +
                (p1 * (3.0 * one_minus_t_squared * t)) +
                (p2 * (3.0 * one_minus_t * t_squared )) +
                (p3 * t_cubed);

        vertices->push_back(p_on_curve.x);
        vertices->push_back(p_on_curve.y);
        vertices->push_back(p_on_curve.z);
    }
}



void GLWidget::makeCubicBezierCurveNormals(std::vector<GLfloat>* normals, point p0, point p1, point p2, point p3, int num_segments)
{
    // number of evaluated points is number of segments + 1

    // Cubic bezier formula derivative = 3(1-t)^2 * (p1 - p0) + 6(1-t)*t*(p2 - p1) + 3t^2 * (p3 - p2)


    // formula for normal is (-y'(t), x(t)

    for(int i = 0; i <= num_segments; ++i)
    {
        float t = i/(float)num_segments;

        float one_minus_t = 1 - t;
        float one_minus_t_squared = one_minus_t * one_minus_t;
        float t_squared = t*t;

        point derivative = (p1 - p0) * (3 * one_minus_t_squared)  +
                (p2-p1)*(6*one_minus_t * t) +
                (p3-p2) * (3*t_squared);

        point n(-derivative.y, derivative.x, 0);


        n = n.normalize();


        normals->push_back(n.x);
        normals->push_back(n.y);
        normals->push_back(n.z);
    }
}

void GLWidget::makeTangentsToPath(BezierCurve& curve)
{

    point p0 = curve.controlPointVector[0];
    point p1 = curve.controlPointVector[1];
    point p2 = curve.controlPointVector[2];
    point p3 = curve.controlPointVector[3];


    curve.pathDerivatives.clear();

    int num_segments = curve.numberOfSegments;


    for(int i = 0; i <= num_segments; ++i)
    {
        float t = i * 1.0/(float)num_segments;

        float one_minus_t = 1 - t;
        float one_minus_t_squared = one_minus_t * one_minus_t;
        float t_squared = t*t;


        point derivative = (p1 - p0) * (3 * one_minus_t_squared)  +
                (p2-p1)*(6*one_minus_t * t) +
                (p3-p2) * (3*t_squared);

        derivative.normalize();


        curve.pathDerivatives.push_back(derivative.x);
        curve.pathDerivatives.push_back(derivative.y);
        curve.pathDerivatives.push_back(derivative.z);
    }
}


void GLWidget::makeBezierSurface(std::vector<GLfloat>* vertices_and_normals,
                                 std::vector<GLubyte>* indices,
                                 std::vector<GLfloat>* textureCoordinates,
                                 point p0, point p1, point p2, point p3,
                                 int num_segments)
{

    std::vector<GLfloat> curve_vertices;
    std::vector<GLfloat> curve_normals;

    vertices_and_normals->clear();
    indices->clear();
    textureCoordinates->clear();

    makeCubicBezierCurve(&curve_vertices, p0, p1, p2,p3, num_segments);
    makeCubicBezierCurveNormals(&curve_normals, p0, p1, p2,p3, num_segments);

    float radius = 2;


    //extrude in the circleyyy direction
    for(int i = 0; i <= num_segments; ++i)
    {
        float v = 1 - i/(float)num_segments;

        //loop over curve vertices
        for(unsigned int j = 0; j < curve_vertices.size(); j += 3)
        {
            float u = (j/3)/(float)num_segments;

            float offset = 2.0*i/(float)num_segments;

            glm::vec3 point = glm::vec3(curve_vertices[j], curve_vertices[j+1], curve_vertices[j+2]);
            glm::vec3 normal_point = glm::vec3(curve_normals[j],curve_normals[j+1], curve_normals[j+2]);


            point =  glm::rotate(point, glm::pi<float>() * offset , glm::vec3(0,1,0));
            normal_point =  glm::rotate(normal_point, glm::pi<float>() * offset , glm::vec3(0,1,0));


            //float x = radius * cos(offset * PI);
            //float y = radius * sin(offset * PI);

            //Vertices
            vertices_and_normals->push_back(point.x);
            vertices_and_normals->push_back(point.y);
            vertices_and_normals->push_back(point.z);

            //Normals
            vertices_and_normals->push_back(normal_point.x);
            vertices_and_normals->push_back(normal_point.y);
            vertices_and_normals->push_back(normal_point.z);

            //textures
            textureCoordinates->push_back(u);
            textureCoordinates->push_back(v);

        }
    }




    for(int i = 0; i < num_segments; ++i)
    {
        for(int j = 0; j < num_segments; ++j)
        {

            //triangle 1 - changed
            indices->push_back(i*(num_segments+1)+j+1);
            indices->push_back((i+1)*(num_segments+1)+j);
            indices->push_back(i*(num_segments+1)+j);

            //triangle 2 - changes
            indices->push_back(i*(num_segments+1)+j+1);
            indices->push_back((i+1)*(num_segments+1)+j+1);
            indices->push_back((i+1)*(num_segments+1)+j);

        }
    }
}




void GLWidget::makeBezierExtremes(
        std::vector<GLfloat>* vertices_and_normals,
        std::vector<GLubyte>* indices,
        std::vector<GLfloat>* textureCoordinates,
        point p0, point p1, point p2, point p3,
        int num_segments, bool isTop)
{

    vertices_and_normals->clear();
    indices->clear();
    textureCoordinates->clear();

    //normal is up if its the top, else its down
    glm::vec3 normal = isTop? glm::vec3(0,1,0) : glm::vec3(0,-1,0);


    std::vector<GLfloat> curve_vertices;
    makeCubicBezierCurve(&curve_vertices, p0, p1, p2,p3, num_segments);


    std::vector<GLfloat> curve_normals;
    makeCubicBezierCurveNormals(&curve_normals,p0,p1,p2,p3,num_segments);

    //loop over curve vertices
    int j = isTop? 0 : curve_vertices.size()-3;

    //extrude in the z direction
    for(int i = 0; i <= num_segments; ++i)
    {
        float v = 1 - i/(float)num_segments;

        float offset = 2.0*i/(float)num_segments;

        glm::vec3 point = glm::vec3(curve_vertices[j], curve_vertices[j+1], curve_vertices[j+2]);

        glm::vec3 point_pos =  glm::rotate(point, glm::pi<float>() * offset , glm::vec3(0,1,0));
        //glm::vec3 normal_points = glm::rotate(point,glm::pi<float>() * offset , glm::vec3(0,1,0));


        float u = 0;

        //Vertice in the revolution
        vertices_and_normals->push_back(point_pos.x);
        vertices_and_normals->push_back(point_pos.y);
        vertices_and_normals->push_back(point_pos.z);


        //Normal for Revolution
        vertices_and_normals->push_back(normal.x);
        vertices_and_normals->push_back(normal.y);
        vertices_and_normals->push_back(normal.z);

        textureCoordinates->push_back(u);
        textureCoordinates->push_back(v);


        u = (1/3)/(float)num_segments;

        //Vertice - the center is at the same height in the center
        vertices_and_normals->push_back(0);
        vertices_and_normals->push_back(point.y);
        vertices_and_normals->push_back(0);

        //Normal for the center
        vertices_and_normals->push_back(normal.x);
        vertices_and_normals->push_back(normal.y);
        vertices_and_normals->push_back(normal.z);

        textureCoordinates->push_back(u);
        textureCoordinates->push_back(v);

    }

    for(int i = 0; i < num_segments*2; i+=2)
    {
        if(isTop){
            //triangle 1
            indices->push_back(i+2);
            indices->push_back(i+1);
            indices->push_back(i);
        }else{
            indices->push_back(i);

            indices->push_back(i+1);
            indices->push_back(i+2);

        }

    }
}


void GLWidget::evaluateCamera(){
    //view_mat *= camRotate;
    view_mat = glm::lookAt(cameraPosition, cameraCenterOfAttention ,cameraUpVector);

    //lighting Shader
    shader_spotlight->bind();
    GLuint loc = glGetUniformLocation(shader_spotlight->programId(), "view_mat");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(view_mat));


    //lighting Shader
    shader_lighting->bind();
    loc = glGetUniformLocation(shader_lighting->programId(), "view_mat");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(view_mat));

    //basic Shader
    shader_basic->bind();
    loc = glGetUniformLocation(shader_basic->programId(), "view_mat");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(view_mat));

}


void GLWidget::makePiecewiseObject(point p0, point p1, point p2, point p3, point p4, point p5, point p6){

    allObjects.append(new BezierObject(new BezierCurve(
                                           p0,
                                           p1,
                                           p2,
                                           p3)));

    allObjects.append(new BezierObject(new BezierCurve(
                                           p3,
                                           p4,
                                           p5,
                                           p6
                                           )));
}

void GLWidget::makePiecewiseObject(point p0, point p1, point p2, point p3){

    allObjects.append(new BezierObject(new BezierCurve(
                                           p0,
                                           p1,
                                           p2,
                                           p3)));
}

void GLWidget::initializePathToWalkOn(){




    pathToWalk = new BezierCurve(
                point( -15,  -.99,   -1),
                point( -15,  -.99,  -15),
                point(  15,  -.99,  -15),
                point(  15,  -.99,    0),
                300);

    makeTangentsToPath(*pathToWalk);


    BezierCurve* secondPartOfCurve = new BezierCurve(
                point(  15,  -.99 ,  0),
                point(  15,  -.99,   15),
                point( -15,  -.99,  15),
                point(  -15,  -.99, -1),
                300);
    makeTangentsToPath(*secondPartOfCurve);


    int originalSizeOfPath = pathToWalk->vertices.size();

    for(int i  = 0; i < secondPartOfCurve->vertices.size(); i++){
        pathToWalk->vertices.append(secondPartOfCurve->vertices.at(i));
        pathToWalk->pathDerivatives.append(secondPartOfCurve->pathDerivatives.at(i));

    }

    //cout << "right before making path rotations" << endl;

    //making all the angles the robot needs to turn
    makepathFacingRotations();

    //cout << "right after making facing rotations" << endl;


    //exit(1);

    glGenVertexArrays(1, &(pathToWalk->vertex_array_id));

    //set the vertex array to be the active one
    glBindVertexArray(pathToWalk->vertex_array_id);

    //get a unique id for the buffer
    glGenBuffers(1, &(pathToWalk->curve_position_buffer));

    //set the vertex buffer to be active
    glBindBuffer(GL_ARRAY_BUFFER, pathToWalk->curve_position_buffer);

    //specify the size and type of the vertex buffer
    glBufferData(GL_ARRAY_BUFFER, (pathToWalk->vertices.size()*sizeof(GLfloat)), pathToWalk->vertices.data(), GL_STATIC_DRAW);

    //set the size of the vertex (3 component) and connect it to the correct shader variable
    GLuint loc = glGetAttribLocation(shader_basic->programId(), "position");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);


    GLuint loc_color = glGetUniformLocation(shader_basic->programId(), "material_color");
    glUniform4f(loc_color, 0, 0, 0, 1);
}

void GLWidget::initializeBuffers()
{

    shader_basic->bind();
    initializePathToWalkOn();

    //------------------------------------------------------------------------------
    //making the ground plane
    //------------------------------------------------------------------------------
    shader_spotlight->bind();
    QVector<GLfloat> vertices;
    vertices.clear();

    QVector<GLfloat> normals;
    normals.clear();

    vertices.push_back(-25); vertices.push_back(-1); vertices.push_back(15);
    vertices.push_back(25); vertices.push_back(-1); vertices.push_back(15);
    vertices.push_back(-25); vertices.push_back(-1); vertices.push_back(-15);
    vertices.push_back(25); vertices.push_back(-1); vertices.push_back(-15);

    normals.push_back(0); normals.push_back(1); normals.push_back(0);
    normals.push_back(0); normals.push_back(1); normals.push_back(0);
    normals.push_back(0); normals.push_back(1); normals.push_back(0);
    normals.push_back(0); normals.push_back(1); normals.push_back(0);



    //get a unique id for the vertex array
    glGenVertexArrays(1, &plane_vertex);
    //set the vertex array to be the active one
    glBindVertexArray(plane_vertex);

    //get a unique id for the position vertex buffer
    glGenBuffers(1, &plane_position);
    //set the position vertex buffer to be active
    glBindBuffer(GL_ARRAY_BUFFER, plane_position);
    //specify the size and type of the position vertex buffer and load data
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vertices.size(), &vertices[0], GL_STATIC_DRAW);

    //set the size of the position vector (3 component) and connect it to the "position" shader variable
    GLuint loc = glGetAttribLocation(shader_spotlight->programId(), "position");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);

    //get a unique id for the normal vertex buffer
    glGenBuffers(1, &plane_normals);
    //set the normal vertex buffer to be active
    glBindBuffer(GL_ARRAY_BUFFER, plane_normals);
    //specify the size and type of the normal vertex buffer and load data
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*normals.size(), &normals[0], GL_STATIC_DRAW);

    //set the size of the normal vector (3 component) and connect it to the "normal" shader variable
    loc = glGetAttribLocation(shader_spotlight->programId(), "normal");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, 0);

    //--------------------------------------------------------------------------------
    //Making the Robot
    //--------------------------------------------------------------------------------



    if(drawRobot){
        shader_lighting->bind();

        allObjects.append(new BezierObject( new BezierCurve(
                                                point(ctrl_pt0.x, ctrl_pt0.y, ctrl_pt0.z),
                                                point(ctrl_pt1.x, ctrl_pt1.y, ctrl_pt1.z),
                                                point(ctrl_pt2.x, ctrl_pt2.y, ctrl_pt2.z),
                                                point(ctrl_pt3.x, ctrl_pt3.y, ctrl_pt3.z)
                                                )));

        //Head
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0.02, 0.9375, 0),
                                                point(0.156, 0.555556, 0),
                                                point(0.62, 0.152778, 0),
                                                point(0.328, -0.395833, 0)
                                                )));


        //Neck
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0.328, 0.701389, 0),
                                                point(0.068, 0.152778, 0),
                                                point(0.28, 0.291667, 0),
                                                point(0.748, 0.05, 0)
                                                )));

        //Body
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0.728, 1.05968, 0),
                                                point(2.588, 0.758871, 0),
                                                point(0.8, 0.2, 0),
                                                point(0.972, -1.08145, 0)
                                                )));

        //Left Leg - thigh
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0.188, 0.944444, 0),
                                                point(0.32, 0.583333, 0),
                                                point(0.296, 0.277778, 0),
                                                point(0.35, -0.95, 0)
                                                )));

        //Left Leg - shin
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0.144, 0.45, 0),
                                                point(0.208, -0.236111, 0),
                                                point(0.304, -0.993056, 0),
                                                point(0.596, -1.07917, 0)
                                                )));

        //Right Leg - thigh
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0.188, 0.944444, 0),
                                                point(0.32, 0.583333, 0),
                                                point(0.296, 0.277778, 0),
                                                point(0.35, -0.95, 0)
                                                )));





        //Right Leg - shin
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0.144, 0.45, 0),
                                                point(0.208, -0.236111, 0),
                                                point(0.304, -0.993056, 0),
                                                point(0.596, -1.07917, 0)
                                                )));

        //Left Arm - bicep
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0, 0.866935, 0),
                                                point(0.668, 0.66129, 0),
                                                point(-0.056, 0.491935, 0),
                                                point(0.172, -0.415323, 0)
                                                )));

        //Left Arm - forearm
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0.072, 0.612903, 0),
                                                point(0.516, -0.971774, 0),
                                                point(0.154, -0.991935, 0),
                                                point(0.02, -0.983871, 0)
                                                )));

        //Right Arm - bicep
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0, 0.866935, 0),
                                                point(0.668, 0.66129, 0),
                                                point(-0.056, 0.491935, 0),
                                                point(0.172, -0.415323, 0)
                                                )));
        //Right Arm - forearm
        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0.072, 0.612903, 0),
                                                point(0.516, -0.971774, 0),
                                                point(0.154, -0.991935, 0),
                                                point(0.02, -0.983871, 0)
                                                )));

        allObjects.append(new BezierObject( new BezierCurve(
                                                point(0.348, 0.75, 0),
                                                point(0.016, 0.254032, 0),
                                                point(0.164, 0.258065, 0),
                                                point(0.232, -0.778226, 0)
                                                )));



        for(int k = 0 ; k < allObjects.size(); k++){
            makeSurface(*allObjects.at(k));
        }
    }//if drawRobot
}

void GLWidget::makeSurface(BezierObject& object)
{

    // std::vector<GLfloat> vertices_normals;
    // std::vector<GLubyte> indices;




    //BODY--------------------------------------------------------------------
    //generate vertices, normals, and indices for the cube
    makeBezierSurface(&object.vertices_normals,
                      &object.indices,
                      &object.textureBuffer,
                      object.curve->controlPointVector[0],
                      object.curve->controlPointVector[1],
                      object.curve->controlPointVector[2],
                      object.curve->controlPointVector[3],
                      object.curve->numberOfSegments);



    //cout << "right before doing makeSurfaceHelperIntializer" << endl;
    //initiates the body of the Curve

    makeSurfaceHelperInitilizer(object.vertex_id,

                                object.position_and_normal_buffer_surface,
                                object.vertices_normals,

                                object.index_buffer,
                                object.indices,

                                object.textureBuffer_id,
                                object.textureBuffer,
                                object.num_indices_surface);


    //cout << "right before creating the top" << endl;

    if(drawRobot){

        //TOP--------------------------------------------------------------------
        //Creating the top of the 3d curve
        makeBezierExtremes(&object.vertices_normals_top,
                           &object.indices_top,

                           &object.textureBuffer_top,

                           object.curve->controlPointVector[0],
                           object.curve->controlPointVector[1],
                           object.curve->controlPointVector[2],
                           object.curve->controlPointVector[3],
                           object.curve->numberOfSegments,
                           true
                           );

        //initiates the top of the curve
        makeSurfaceHelperInitilizer(object.vertex_top_id,
                                    object.position_and_normal_buffer_top,
                                    object.vertices_normals_top,
                                    object.index_buffer_top,
                                    object.indices_top,
                                    object.textureBuffer_top_id,
                                    object.textureBuffer_top,
                                    object.num_indices_top);




        //cout << "right before making the bottom" << endl;

        //BOTTOM--------------------------------------------------------------
        //Creating the top of the 3d curve
        makeBezierExtremes(&object.vertices_normals_bottom,
                           &object.indices_bottom,
                           &object.textureBuffer_bottom,
                           object.curve->controlPointVector[0],
                           object.curve->controlPointVector[1],
                           object.curve->controlPointVector[2],
                           object.curve->controlPointVector[3],
                           object.curve->numberOfSegments,
                           false
                           );

        //initiates the top of the curve
        makeSurfaceHelperInitilizer(object.vertex_bottom_id,
                                    object.position_and_normal_buffer_bottom,
                                    object.vertices_normals_bottom,
                                    object.index_buffer_bottom,
                                    object.indices_bottom,
                                    object.textureBuffer_bottom_id,
                                    object.textureBuffer_bottom,
                                    object.num_indices_bottom);

    }


}



void GLWidget::paintBezierObject(
        BezierObject &object,
        glm::vec3 scale,
        glm::vec3 inTranslationMatrix,
        glm::vec3 color,
        float rotationAngle,
        glm::vec3 rotationAxis,
        glm::vec3 topOfAppendageIn,
        bool useTopOfAppendage,
        bool keepOldModel_matrix,
        GLuint textureToUse,
        int index
        ){




    //Drawing the surface----------------------------------------------------------

    glBindVertexArray(object.vertex_id);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureToUse);




    glm::vec3 tangentDirection = glm::vec3(pathToWalk->pathDerivatives.at(pointIterator),
                                           pathToWalk->pathDerivatives.at(pointIterator+1),
                                           pathToWalk->pathDerivatives.at(pointIterator+2));


    glm::vec3 currentDirection = currentRobotDirection;

    float magnitudeTangentDirection = sqrt( (tangentDirection.x * tangentDirection.x)
                                            + (tangentDirection.y * tangentDirection.y)
                                            + (tangentDirection.z * tangentDirection.z));

    float magnitudeCurrentDirection = sqrt( (currentDirection.x * currentDirection.x)
                                            + (currentDirection.y * currentDirection.y)
                                            + (currentDirection.z * currentDirection.z));

    float multipliedMagnitudes = magnitudeTangentDirection * magnitudeCurrentDirection;

    float dotTangentCurrent = glm::dot(tangentDirection, currentDirection);









    float facingPathAngle = qAcos(dotTangentCurrent / multipliedMagnitudes);

    if(pointIterator > pathToWalk->vertices.size()/2){
        facingPathAngle *= -1;
    }


    //float facingPathAngle = pathToWalk->pathRotationAngles.at(pointIterator);

    //cout << "angle: " << facingPathAngle << endl;

    glm::vec3 bodyPosition = posMatPaint.at(BODY);


    //Placement Things------------------------------------------------
    if(useTopOfAppendage ){

        if(keepOldModel_matrix){
            //Translating to the top of the appendage
            model_mat = glm::translate(model_mat, topOfAppendageIn - posMatPaint.at(index-1));

            //Doing our Animation
            model_mat = glm::rotate(model_mat, glm::pi<float>() * rotationAngle,rotationAxis);

            model_mat = glm::translate(model_mat, inTranslationMatrix - topOfAppendageIn);

            if(1){
                if(index == FLASHLIGHT){
                    shader_spotlight->bind();
                    GLuint loc;

                    //---------------------------------------------------------------------------
                    //Uploading the model_mat
                    //---------------------------------------------------------------------------
                    loc = glGetUniformLocation(shader_spotlight->programId(), "model_mat");
                    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(model_mat));



                    //----------------------------------------------------------------------------
                    //direction for spotlight
                    //-----------------------------------------------------------------------------
                    spot_direction = glm::vec3(pathToWalk->pathDerivatives.at(pointIterator),
                                               pathToWalk->pathDerivatives.at(pointIterator+1),
                                               pathToWalk->pathDerivatives.at(pointIterator+2));

                    loc = glGetUniformLocation(shader_spotlight->programId(), "spot_dir");
                    glUniform3f(loc, spot_direction.x,
                                spot_direction.y-15,
                                spot_direction.z);







                    //-----------------------------------------------------------
                    //Positioning
                    //-------------------------------------------------------------
                    spot_position.x = model_mat[3][0];
                    spot_position.y = model_mat[3][1];
                    spot_position.z = model_mat[3][2];

                    loc = glGetUniformLocation(shader_spotlight->programId(), "spot_pos");
                    glUniform3f(loc, spot_position.x, spot_position.y, spot_position.z);


                    normal_mat = glm::mat3(view_mat * model_mat);
                    GLuint loc_norm = glGetUniformLocation(shader_spotlight->programId(), "normal_mat");
                    glUniformMatrix3fv(loc_norm, 1, GL_FALSE, glm::value_ptr(normal_mat));





                    shader_lighting->bind();


                }
            }else{
                cout << "just rotated" << endl;
                justRotatedTheCamera = false;
            }

        }



        else{

            //Translate to the body
            model_mat = glm::translate(glm::mat4(1), bodyPosition);

            model_mat =  rotationMatrix * model_mat ;
            model_mat = glm::rotate(model_mat, facingPathAngle , glm::vec3(0,1,0));

            model_mat = glm::translate(model_mat, topOfAppendageIn - bodyPosition);



            model_mat = glm::rotate(model_mat, glm::pi<float>() * rotationAngle,rotationAxis);


            glm::vec3 netDifference = inTranslationMatrix - topOfAppendageIn;
            model_mat = glm::translate(model_mat, netDifference);
        }




    }//if using the top appendage


    else{//not using the top appendage

        if(keepOldModel_matrix){
            model_mat = glm::rotate(model_mat, facingPathAngle , glm::vec3(0,1,0));

            model_mat = glm::rotate(model_mat, glm::pi<float>() * rotationAngle,
                                    rotationAxis);

            model_mat = glm::translate(model_mat, inTranslationMatrix - posMatPaint.at(index-1));

        }else{
            //Translate back to the intended position
            model_mat = glm::translate(glm::mat4(1), inTranslationMatrix );
            model_mat =  rotationMatrix * model_mat ;

            //animation
            model_mat = glm::rotate(model_mat, glm::pi<float>() * rotationAngle,rotationAxis);

        }



    }

    model_mat = glm::scale(model_mat, scale);

    //=============================================================


    GLuint loc_model = glGetUniformLocation(shader_lighting->programId(), "model_mat");

    glUniformMatrix4fv(loc_model, 1, GL_FALSE, glm::value_ptr(model_mat));

    normal_mat = glm::mat3(view_mat * model_mat);

    GLuint loc_norm = glGetUniformLocation(shader_lighting->programId(), "normal_mat");

    glUniformMatrix3fv(loc_norm, 1, GL_FALSE, glm::value_ptr(normal_mat));

    GLuint loc_color = glGetUniformLocation(shader_lighting->programId(), "material_color");

    glUniform4f(loc_color, color.r, color.g, color.b, 1.0);



    glDrawElements(GL_TRIANGLES, object.num_indices_surface, GL_UNSIGNED_BYTE, (GLvoid*)0);


    //DRAWING THE TOP------------------------------------------------------------------------

    glBindVertexArray(object.vertex_top_id);


    //=============================================================

    loc_model = glGetUniformLocation(shader_lighting->programId(), "model_mat");

    glUniformMatrix4fv(loc_model, 1, GL_FALSE, glm::value_ptr(model_mat));

    normal_mat = glm::mat3(view_mat * model_mat);

    loc_norm = glGetUniformLocation(shader_lighting->programId(), "normal_mat");

    glUniformMatrix3fv(loc_norm, 1, GL_FALSE, glm::value_ptr(normal_mat));

    loc_color = glGetUniformLocation(shader_lighting->programId(), "material_color");

    glUniform4f(loc_color, color.r, color.g, color.b, 1.0);

    glDrawElements(GL_TRIANGLES, object.num_indices_top, GL_UNSIGNED_BYTE, (GLvoid*)0);


    //DRAWING THE BOTTOM--------------------------------------------------------

    glBindVertexArray(object.vertex_bottom_id);






    //=============================================================

    loc_model = glGetUniformLocation(shader_lighting->programId(), "model_mat");

    glUniformMatrix4fv(loc_model, 1, GL_FALSE, glm::value_ptr(model_mat));

    normal_mat = glm::mat3(view_mat * model_mat);

    loc_norm = glGetUniformLocation(shader_lighting->programId(), "normal_mat");

    glUniformMatrix3fv(loc_norm, 1, GL_FALSE, glm::value_ptr(normal_mat));

    loc_color = glGetUniformLocation(shader_lighting->programId(), "material_color");

    glUniform4f(loc_color, color.r, color.g, color.b, 1.0);

    glDrawElements(GL_TRIANGLES, object.num_indices_bottom, GL_UNSIGNED_BYTE, (GLvoid*)0);

    glDisable(GL_TEXTURE_2D);

}
void GLWidget::timerFunc(){

    if(animationOn){

        animationCounter += 0.1;

        anim_time += anim_step;

        pointOnCurve = glm::vec3(
                    pathToWalk->vertices.at(pointIterator),
                    pathToWalk->vertices.at(pointIterator+1),
                    pathToWalk->vertices.at(pointIterator+2)
                    );







        //printVec3(pointOnCurve);
        //cout << pointIterator << endl;

        pointIterator += 3;
        pointIterator %= pathToWalk->vertices.size();


        updateGL();
    }
}

void GLWidget::drawPath(){

    //Select the curves vertex array


    glBindVertexArray(pathToWalk->vertex_array_id);



    //set the model Matrix
    model_mat = glm::mat4(1);
    model_mat =  rotationMatrix * model_mat ;
    GLuint loc_model = glGetUniformLocation(shader_basic->programId(), "model_mat");
    glUniformMatrix4fv(loc_model, 1, GL_FALSE, glm::value_ptr(model_mat));


    GLuint loc_color = glGetUniformLocation(shader_basic->programId(), "material_color");
    glUniform4f(loc_color, 0.0, 0.0, 0.0, 1.0);



    //drawing the curve
    glDrawArrays(GL_LINE_STRIP, 0, pathToWalk->vertices.size()/3);

}

void GLWidget::drawPlane(){

    glBindVertexArray(plane_vertex);

    model_mat = glm::mat4(1) * rotationMatrix;

    GLuint loc = glGetUniformLocation(shader_spotlight->programId(), "model_mat");
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(model_mat));

    normal_mat = glm::mat3(view_mat*model_mat);
    GLuint loc_norm = glGetUniformLocation(shader_spotlight->programId(), "normal_mat");
    glUniformMatrix3fv(loc_norm, 1, GL_FALSE, glm::value_ptr(normal_mat));


    loc = glGetUniformLocation(shader_spotlight->programId(), "material_color");
    glUniform4f(loc, 1, 1, 1, 1);

    //draw the plane
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void GLWidget::paintGL()
{

    //clear the color and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    shader_basic->bind();
    drawPath();


    shader_spotlight->bind();
    drawPlane();



    if(drawRobot){

        shader_lighting->bind();

        posMatPaint.clear();
        scaleMatPaint.clear();
        colorMatPaint.clear();
        rotationAngles.clear();
        rotMatAxis.clear();

        topOfAppendage.clear();
        useTopOfAppendage.clear();
        keepOldModelMatrix.clear();



        //Test Object
        posMatPaint.append(glm::vec3(bookX,bookY,0));
        scaleMatPaint.append(glm::vec3(1,1,1));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(0.0);
        rotMatAxis.append(glm::vec3(0,1,0));
        topOfAppendage.append(glm::vec3(0,0,0));
        useTopOfAppendage.append(false);
        keepOldModelMatrix.append(false);


        //Head
        posMatPaint.append(glm::vec3(0,5,0));
        scaleMatPaint.append(glm::vec3(1,1,.75));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(0.0);
        rotMatAxis.append(glm::vec3(0,1,0));
        topOfAppendage.append(glm::vec3(0,0,0));
        useTopOfAppendage.append(false);
        keepOldModelMatrix.append(false);


        //Neck
        posMatPaint.append(glm::vec3(0,4,0));
        scaleMatPaint.append(glm::vec3(1,1,.5));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(0.0);
        rotMatAxis.append(glm::vec3(0,1,0));
        topOfAppendage.append(glm::vec3(0,0,0));
        useTopOfAppendage.append(false);
        keepOldModelMatrix.append(false);


        //Body
        posMatPaint.append(glm::vec3(0,3,0));
        scaleMatPaint.append(glm::vec3(1,1,1));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(0.0);
        rotMatAxis.append(glm::vec3(0,1,0));
        topOfAppendage.append(glm::vec3(0,0,0));
        useTopOfAppendage.append(false);
        keepOldModelMatrix.append(false);


        //Left Leg - thigh
        posMatPaint.append(glm::vec3(-.6,1.4,0));
        scaleMatPaint.append(glm::vec3(1,1,1));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(sin(animationCounter) * .20);
        rotMatAxis.append(glm::vec3(1,0,0));
        topOfAppendage.append(glm::vec3(-.6,1.6,0));
        useTopOfAppendage.append(true);
        keepOldModelMatrix.append(false);

        //Left Leg - Shin
        posMatPaint.append(glm::vec3(-.6,0,0));
        scaleMatPaint.append(glm::vec3(1,1,1));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(-sin(animationCounter) * 0.172 + .05 );
        rotMatAxis.append(glm::vec3(1,0,0));
        topOfAppendage.append(glm::vec3(-0.6,0.4,0));
        useTopOfAppendage.append(true);
        keepOldModelMatrix.append(true);


        //Right Leg - thigh
        posMatPaint.append(glm::vec3(.6,1.2,0));
        scaleMatPaint.append(glm::vec3(1,1,1));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(-sin(animationCounter) * .20);
        rotMatAxis.append(glm::vec3(1,0,0));
        topOfAppendage.append(glm::vec3(.6,1.6,0));
        useTopOfAppendage.append(true);
        keepOldModelMatrix.append(false);



        //Right Leg - Shin
        posMatPaint.append(glm::vec3(.6,0,0));
        scaleMatPaint.append(glm::vec3(1,1,1));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(sin(animationCounter) * 0.172 + .05 );
        rotMatAxis.append(glm::vec3(1,0,0));
        topOfAppendage.append(glm::vec3(0.6,0.4,0));
        useTopOfAppendage.append(true);
        keepOldModelMatrix.append(true);



        //Left Arm - Bicep
        posMatPaint.append(glm::vec3(-1.75,3,0));
        scaleMatPaint.append(glm::vec3(1,1.2,1));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(-sin(animationCounter) * .15);
        rotMatAxis.append(glm::vec3(1,0,0));
        topOfAppendage.append(glm::vec3(-1.75,3.4,0));
        useTopOfAppendage.append(true);
        keepOldModelMatrix.append(false);




        //Left Arm - forearm
        posMatPaint.append(glm::vec3(-1.75,2.0,0.0));
        scaleMatPaint.append(glm::vec3(1,1,1));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(-sin(animationCounter) * .30 - .20);
        rotMatAxis.append(glm::vec3(1,0,0));
        topOfAppendage.append(glm::vec3(-1.75,2.52,0));
        useTopOfAppendage.append(true);
        keepOldModelMatrix.append(true);




        //Right Arm - Bicep
        posMatPaint.append(glm::vec3(1.75,3,0));
        scaleMatPaint.append(glm::vec3(1,1.2,1));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(sin(animationCounter) * .15);
        rotMatAxis.append(glm::vec3(1,0,0));
        topOfAppendage.append(glm::vec3(1.75,3.4,0));
        useTopOfAppendage.append(true);
        keepOldModelMatrix.append(false);




        //Right Arm - forearm
        posMatPaint.append(glm::vec3(1.75,2.0,0));
        scaleMatPaint.append(glm::vec3(1,1,1));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(-.4);//sin(animationCounter) * .30 - .20);
        rotMatAxis.append(glm::vec3(1,0,0));
        topOfAppendage.append(glm::vec3(1.75,2.52,0));
        useTopOfAppendage.append(true);
        keepOldModelMatrix.append(true);

        //FlashLight
        posMatPaint.append(glm::vec3(1.75,1.0,0));
        scaleMatPaint.append(glm::vec3(1,1,.4));
        colorMatPaint.append(glm::vec3(0,0,0));
        rotationAngles.append(bookX);//sin(animationCounter) * .05);
        rotMatAxis.append(glm::vec3(1,0,0));
        topOfAppendage.append(glm::vec3(1.75,1.4,0));
        useTopOfAppendage.append(true);
        keepOldModelMatrix.append(true);

        shader_spotlight->bind();
        /*
        //spotlight position
        GLuint loc = glGetUniformLocation(shader_spotlight->programId(), "spot_pos");
        glUniform3f(loc, posMatPaint.at(FLASHLIGHT).x + pathToWalk->vertices.at(pointIterator),
                    posMatPaint.at(FLASHLIGHT).y + pathToWalk->vertices.at(pointIterator+1),
                    posMatPaint.at(FLASHLIGHT).z + pathToWalk->vertices.at(pointIterator+2));
*/





        //paintBezierObject(*allObjects.at(Test),testScale,testPosition,testColor,flower_texture);




        shader_lighting->bind();
        for(int i = 1 ; i < allObjects.size(); i++){

            glm::vec3 positionWithPath = posMatPaint.at(i);
            glm::vec3 topAppendageWithPath = topOfAppendage.at(i);

            if(i > 0){
                positionWithPath.x += pathToWalk->vertices.at(pointIterator);
                positionWithPath.y += 0;
                positionWithPath.z += pathToWalk->vertices.at(pointIterator+2);
                posMatPaint[i] = positionWithPath;

                topAppendageWithPath.x += pathToWalk->vertices.at(pointIterator);
                topAppendageWithPath.y += 0;
                topAppendageWithPath.z += pathToWalk->vertices.at(pointIterator+2);
                topOfAppendage[i] = topAppendageWithPath;



                if(cameraHead && i == BODY){ //&& pointIterator != 1758 && (pointIterator > 1570
                    //|| pointIterator < 1506) ){

                    cameraPosition = positionWithPath;
                    cameraPosition.x *= .95;
                    cameraPosition.y *= .96;
                    cameraPosition.z *= .94;

                    cameraCenterOfAttention = glm::vec3(
                                pathToWalk->pathDerivatives.at(pointIterator) + cameraHeadX,
                                pathToWalk->pathDerivatives.at(pointIterator+1) + cameraHeadY,
                                pathToWalk->pathDerivatives.at(pointIterator+2)) + cameraHeadZ,

                            cameraCenterOfAttention.x *= 1.5 ;
                    cameraCenterOfAttention.y *= 1.5 ;
                    cameraCenterOfAttention.z *=  1.5;


                    view_mat = glm::lookAt(cameraPosition,cameraCenterOfAttention ,cameraUpVector );

                    shaderCameraSetUp();

                }//if camerahead and body

            }



            paintBezierObject(
                        *allObjects.at(i),
                        scaleMatPaint.at(i),
                        positionWithPath,
                        colorMatPaint.at(i),
                        rotationAngles.at(i),
                        rotMatAxis.at(i),
                        topAppendageWithPath,
                        useTopOfAppendage.at(i),
                        keepOldModelMatrix.at(i),
                        flower_texture, i);
        }
    }
}



void GLWidget::makeSurfaceHelperInitilizer(GLuint& vertex_id,

                                           GLuint& position_and_normal_buffer_surface,
                                           vector<GLfloat>& vertices_normals,

                                           GLuint& index_buffer_id,
                                           vector<GLubyte>& indices,

                                           GLuint& textureBuffer_id,
                                           vector<GLfloat>& textureBuffer,


                                           GLuint& num_indices_surface
                                           ){

    num_indices_surface = indices.size();

    //get a unique id for the vertex array
    glGenVertexArrays(1, &vertex_id);

    //set the vertex array to be the active one
    glBindVertexArray(vertex_id);


    //get a unique id for the position vertex buffer---------------------------------------------
    glGenBuffers(1, &position_and_normal_buffer_surface);

    //set the position vertex buffer to be active
    glBindBuffer(GL_ARRAY_BUFFER, position_and_normal_buffer_surface);

    //specify the size and type of the position vertex buffer and load data
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vertices_normals.size(), vertices_normals.data(), GL_STATIC_DRAW);

    //set the size of the position vector (3 component) and connect it to the "position" shader variable
    GLuint loc = glGetAttribLocation(shader_lighting->programId(), "position");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*6, 0);

    //set the size of the normal vector (3 component) and connect it to the "normal" shader variable
    loc = glGetAttribLocation(shader_lighting->programId(), "normal");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*6, (void*)(sizeof(GLfloat)*3));



    //cout << "right before doing the texture part" << endl;

    //get a buffer for the texture coordinates---------------------------------------------------------
    glGenBuffers(1, &textureBuffer_id);

    //set the texture vertex buffer to be active
    glBindBuffer(GL_ARRAY_BUFFER, textureBuffer_id);

    //specify the size and type of the position vertex buffer and load data
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*textureBuffer.size(), textureBuffer.data(), GL_STATIC_DRAW);

    loc = glGetAttribLocation(shader_lighting->programId(), "tex_coord");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 2, 0);


    glGenBuffers(1, &index_buffer_id);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_id);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte)*indices.size(), indices.data(), GL_STATIC_DRAW);
}

void GLWidget::printControlPoints(){
    cout << endl;
    cout << "The control points are as follows: " << endl;
    for(int i = 0; i < 4; i++){
        cout << "p"<< i << ": (" << ctrl_pt[i]->x << ", " << ctrl_pt[i]->y << ", " <<
                ctrl_pt[i]->z << ")" << endl;
    }//for
    cout << "\n";

    cout << "allObjects.append(new BezierObject( new BezierCurve(" << endl;
    int i = 0;
    for(; i < 3; i++){
        cout << "point(" << ctrl_pt[i]->x << ", " << ctrl_pt[i]->y << ", " <<
                ctrl_pt[i]->z << ")," << endl;
    }
    cout << "point(" << ctrl_pt[i]->x << ", " << ctrl_pt[i]->y << ", " <<
            ctrl_pt[i]->z << ")" << endl;
    cout << ")));\n" << endl;

    cout << "posMatPaint.append(glm::vec3(" << bookX << "," << bookY << ",0));" << endl;
}

void GLWidget::printVec3(glm::vec3 vectorIn)
{
    cout << "X: " << vectorIn.x << " Y: " << vectorIn.y << " Z: " << vectorIn.z << endl;
}

void GLWidget::printVertices(QString title, QVector<GLfloat>& verticesIn){

    cout << "Printing Vertices ( "<< title.data() << " ): " << endl;
    for(int i = 0 ; i < verticesIn.size(); i+= 3){
        cout << "(" << verticesIn.at(i) << ", " << verticesIn.at(i+1) << ", " <<
                verticesIn.at(i+2) << ")" << endl;
    }
}
void GLWidget::printVertices(QString title, QVector<int>& verticesIn){

    cout << "Printing Vertices ( "<< title.data() << " ): " << endl;
    for(int i = 0 ; i < verticesIn.size(); i+= 3){
        cout << "(" << verticesIn.at(i) << ", " << verticesIn.at(i+1) << ", " <<
                verticesIn.at(i+2) << ")" << endl;
    }
}

void GLWidget::printPoints(point p){
    cout << "Printing Point: " << "( " <<
            p.x << ", " << p.y << ", " << p.z << ")" << endl;
}


float sign = -1;
int rotatIndex = 0;
bool set = false;

void GLWidget::makepathFacingRotations(){
    //cout << "im in making path function rotations" << endl;

    for(int i = 0; i < pathToWalk->pathDerivatives.size(); i+=3){

        glm::vec3 a = currentRobotDirection;

        glm::vec3 b = glm::vec3(pathToWalk->pathDerivatives.at(i),
                                pathToWalk->pathDerivatives.at(i+1),
                                pathToWalk->pathDerivatives.at(i+2));

        float dotAB = glm::dot(a,b);

        float magnitudeA = sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
        float magnitudeB = sqrt(b.x*b.x + b.y*b.y + b.z*b.z);

        float Theta = dotAB/(magnitudeA*magnitudeB);

        if(!set && rotatIndex > 0 &&
                abs(pathToWalk->pathRotationAngles.at(rotatIndex - 1)) > abs(glm::acos(Theta))){
            sign = 1;
            set = true;
        }

        pathToWalk->pathRotationAngles.push_back(sign*glm::acos(Theta));
        rotatIndex++;
    }
}



void GLWidget::rotateCamera(float angle, glm::vec3 axis){

    /*
        //Defining our camera as positioned at (0,3,5), looking at (0,0,0) and
        //having the top of camera towards the Y Axis
        this->cameraPosX = 0;
        this->cameraPosY = 45;
        this->cameraPosZ = 40;

        cameraPosition = glm::vec4(cameraPosition);
        cameraPosition


        cameraPosition = glm::vec3(cameraPosX,cameraPosY,cameraPosZ);
        cameraCenterOfAttention = glm::vec3(0,0,0);
        cameraUpVector = glm::vec3(0,1,0);
        view_mat = glm::lookAt(cameraPosition,cameraCenterOfAttention ,cameraUpVector );


        */

}












































